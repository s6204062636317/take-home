import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Flight } from '../flight';
import { PageService } from '../service/page.service';


@Component({
  selector: 'app-flight',
  templateUrl: './flight.component.html',
  styleUrls: ['./flight.component.css']
})
export class FlightComponent implements OnInit {

  flights !: Flight[];
  Booking_Form !: FormGroup;
  constructor(private fb: FormBuilder, private pageService: PageService) { }

  ngOnInit(): void {
    this.Booking_Form = this.fb.group({
      Name:['', [Validators.required]],
      From:['', [Validators.required]],
      To:['', [Validators.required]],
      Type:['', [Validators.required]],
      Depart:['', [Validators.required]],
      Arrival:['',[Validators.required]],
      Adult:['', [Validators.required, Validators.min(1)]],
      Children:['', [Validators.required,  Validators.min(0)]],
      Infants:['',[Validators.required,  Validators.min(0)]]

    });
    this.getFlightPage();
  }
  getFlightPage() {
    this.flights = this.pageService.getFlights();
  }
  onsubmit(f:FormGroup): void{
    let form_record =new Flight(
      f.get('Name')?.value,
      f.get('From')?.value,
      f.get('To')?.value,
      f.get('Type')?.value,
      f.get('Depart')?.value,
      f.get('Arrival')?.value,
      f.get('Adult')?.value,
      f.get('Children')?.value,
      f.get('Infants')?.value)
      this.pageService.addFlights(form_record);
  }
}
