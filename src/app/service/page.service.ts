import { Injectable } from '@angular/core';
import { Flight } from '../flight';
import { Mockflight } from '../share/mockflight';

@Injectable({
  providedIn: 'root'
})

export class PageService {
  flights : Flight[] = [];
  constructor() {
    this.flights = Mockflight.mflight;
  }
  getFlights() : Flight[]{
    return this.flights;
  }
  addFlights(f:Flight): void{
    this.flights.push(f);
  }
}
